Blobs allow transmission of unstructured information in a structured and versioned way.

Blob Version 0
{
    version:0,
    security-provider:"tweetnacl",
    private-key:"base64EncodedUint8",
    master-password:"base64EncodedUint8" <- This means that derived password from user password is used to unlock Blob which then uses master-password to encrypt/decrypt all content
}